from flask import Flask
import requests
import json
from flask import request, jsonify
from datetime import datetime, timezone
import numpy as np
app = Flask(__name__)
# app.config["DEBUG"] = True

def convettoUTC(date, format):
        datetime_object = datetime.strptime(date, format)
        utc_time = datetime_object.replace(tzinfo=timezone.utc)
        utc_timestamp = utc_time.timestamp()
        return utc_timestamp, datetime_object
def convert(seconds): 
    seconds = seconds % (24 * 3600) 
    hour = seconds // 3600
    seconds %= 3600
    minutes = seconds // 60
    seconds %= 60
      
    return "%dh:%02dm:%02ds" % (hour, minutes, seconds)

@app.route('/', methods=['GET'])
def home():
    
    return "<h1>Site is up and Running</h1><p>This site is a prototype API for showing plant functioning.</p>"

@app.route("/getProductionState", methods=["GET"])
def getProductionState():
    if 'startDate' in request.args:
        startDate = request.args['startDate']
    else:
        return 'No start date provided'
    if 'endDate' in request.args:
        endDate = request.args['endDate']
    else:
        return 'No end date provided'
    uri = "https://gitlab.com/-/snippets/2067888/raw/master/sample_json_1.json"
    try:
        uResponse = requests.get(uri)
    except requests.ConnectionError:
       return "Connection Error"  
    Jresponse = uResponse.text
    originalData = json.loads(Jresponse);
    d={
        "shiftA" : ['6:00 AM', '2:00 PM'],
       "shiftB" : ['2:00 PM', '8:00 PM'],
       "shiftC" : ['8:00 PM', '6:00 AM'] 
    }
    for item in originalData:
        itemTime, dateobj = convettoUTC(item['time'], "%Y-%m-%d %H:%M:%S")
        item['utcTime'] = itemTime
        if dateobj.hour >=6 and dateobj.hour <14:
            item['shift'] = 'shiftA'
        elif dateobj.hour >=14 and dateobj.hour < 20:
            item['shift'] = 'shiftB'
        else:
            item['shift'] = 'shiftC'
        # print(dateobj.hour)
        
    
    # utc_time1 = datetime_object1.replace(tzinfo=timezone.utc)
    # utc_timestamp1 = utc_time1.timestamp()
    # print(utc_timestamp1)
    str_time, str_obj = convettoUTC(startDate, "%Y-%m-%dT%H:%M:%SZ")
    end_time, end_obj = convettoUTC(endDate, "%Y-%m-%dT%H:%M:%SZ")
    output = {
	"shiftA" :{ "production_A_count" :0, "production_B_count" :0},
	"shiftB" :{ "production_A_count" :0, "production_B_count" :0},
	"shiftC" :{ "production_A_count" :0, "production_B_count" :0},
}

    for item in originalData:
        if (item['utcTime']>=str_time and item['utcTime']<=end_time):
            if(item['production_A']== True):
                output[item['shift']]['production_A_count'] += 1
            if(item['production_B']== True):
                output[item['shift']]['production_B_count'] += 1
            
    return jsonify(output)
    # return jsonify(data)
    
@app.route("/Question2", methods=["GET"])
def Question2():
    if 'startDate' in request.args:
        startDate = request.args['startDate']
    else:
        return 'No start date provided'
    if 'endDate' in request.args:
        endDate = request.args['endDate']
    else:
        return 'No end date provided'
    uri = "https://gitlab.com/-/snippets/2067888/raw/master/sample_json_2.json"
    try:
        uResponse = requests.get(uri)
    except requests.ConnectionError:
       return "Connection Error"  
    Jresponse = uResponse.text
    originalData = json.loads(Jresponse);
    str_time, str_obj = convettoUTC(startDate, "%Y-%m-%dT%H:%M:%SZ")
    end_time, end_obj = convettoUTC(endDate, "%Y-%m-%dT%H:%M:%SZ")
    output={
        "runtime" : 0,
	    "downtime": 0,
	    "utilisation": 0
    }
    print(originalData)
    for item in originalData:
        itemTime, dateobj = convettoUTC(item['time'], "%Y-%m-%d %H:%M:%S")
        item['utcTime'] = itemTime
        if itemTime>=str_time and itemTime<=end_time:
            if item['runtime']<1021:
                output['runtime']= output['runtime']+item['runtime']
            else:
                output['runtime'] = output['runtime']+1021
                output['downtime']= output['downtime']+(item['runtime']-1021)


    output['utilisation']= output['runtime']/(output['runtime']+output['downtime'])*100
    output['utilisation']=round(output['utilisation'],2)
    output['runtime']=convert(output['runtime'])
    output['downtime']=convert(output['downtime'])
    
    
    return jsonify(output)
@app.route("/Question3", methods=["GET"])
def Question3():
    if 'startDate' in request.args:
        startDate = request.args['startDate']
    else:
        return 'No start date provided'
    if 'endDate' in request.args:
        endDate = request.args['endDate']
    else:
        return 'No end date provided'
    uri = "https://gitlab.com/-/snippets/2067888/raw/master/sample_json_3.json"
    try:
        uResponse = requests.get(uri)
    except requests.ConnectionError:
       return "Connection Error"  
    Jresponse = uResponse.text
    originalData = json.loads(Jresponse);
    str_time, str_obj = convettoUTC(startDate, "%Y-%m-%dT%H:%M:%SZ")
    end_time, end_obj = convettoUTC(endDate, "%Y-%m-%dT%H:%M:%SZ")

    list = []
    for item in originalData:
        itemTime, dateobj = convettoUTC(item['time'], "%Y-%m-%d %H:%M:%S")
        item['utcTime'] = itemTime
        item['id'] = int(item['id'].split('ch')[1])
        if itemTime>=str_time and itemTime<=end_time:
            list.append(item)

    list.sort(key=lambda item: item['id'])
    res = [ sub['id'] for sub in list]
    unique = np.unique(res)
    output = []
    for id in unique:
        the_uniquelist = [d for d in list if d['id'] == id]
        current_entity = {"id" : id, "avg_belt1" : 0, "avg_belt2" : 0}
        for item in the_uniquelist:
            if(item['state']== True):
                current_entity['avg_belt2']+=item['belt2']
            else:
                current_entity['avg_belt1']+=item['belt1']
        current_entity['id'] = int(current_entity['id'])
        current_entity['avg_belt1']= float(current_entity['avg_belt1']/len(the_uniquelist))
        current_entity['avg_belt2']=float(current_entity['avg_belt2']/len(the_uniquelist))
        output.append(current_entity)
    
    return(jsonify(output))
# app.run()